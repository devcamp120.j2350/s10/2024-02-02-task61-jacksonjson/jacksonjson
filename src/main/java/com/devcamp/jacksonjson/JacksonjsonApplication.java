package com.devcamp.jacksonjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksonjsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(JacksonjsonApplication.class, args);
	}

}
