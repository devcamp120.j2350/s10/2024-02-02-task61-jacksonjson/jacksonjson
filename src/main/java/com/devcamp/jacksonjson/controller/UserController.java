package com.devcamp.jacksonjson.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jacksonjson.model.Item;
import com.devcamp.jacksonjson.model.User;

@RestController
@CrossOrigin
public class UserController {

	@GetMapping("/item")
	public Item getItem() {
		User user = new User(1, "John");
		Item item = new Item(2, "book", user);
        Item item2 = new Item(3, "disk", user);
		user.addItem(item);
        user.addItem(item2);
		return item;
	}

    @GetMapping("/user")
	public Item getUser() {
		User user = new User(1, "John");
		Item item = new Item(2, "book", user);
        Item item2 = new Item(3, "disk", user);
		user.addItem(item);
        user.addItem(item2);
		return item;
	}
}
